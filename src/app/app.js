require("../assets/style.css");
require("bootstrap-webpack");

var angular = require('angular');
require("angular-route");
require("angular-ui-bootstrap");
require("./controller/home-controller")
require("./controller/product-controller")
require("./controller/generic-controller")

var app = angular.module('app', [
  'ngRoute',
  'homeController',
  'productController'
]);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: '/src/app/template/home.html',
        controller: 'HomeCtrl'
      }).
      when('/shop/:category', {
        templateUrl: '/src/app/template/shop.html',
        controller: 'shopCtrl'
      }).
      when('/products', {
        templateUrl: '/src/app/template/product.html',
        controller: 'productCtrl'
      }).
      when('/cart/', {
        templateUrl: '/src/app/template/cart.html',
        controller: 'cartCtrl'
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]);