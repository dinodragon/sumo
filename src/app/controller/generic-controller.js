var GenericControllers = angular.module('genericController', []);

GenericControllers.directive('sumoCategory', function() {
  return {
    restrict: 'AEC',
    scope: {
      product: '=info'
    },
    templateUrl: '/src/app/template/category.html'
  };
})

GenericControllers.directive('sumoCartitem', function() {
  return {
    restrict: 'AEC',
    templateUrl: '/src/app/template/cart-item.html'
  };
})

GenericControllers.controller('genericModalCtrl', ['$scope', '$uibModalInstance',
  function ($scope, $uibModalInstance){
    $scope.ok = function () {
      close('ok');
    };

    $scope.close = function () {
      close('cancel');
    };

    var close = function(action){
      $('.modal').fadeOut(200);
      setTimeout(function(){
        $uibModalInstance.dismiss(action);
      }, 180)
    }
  }]);

module.exports = GenericControllers