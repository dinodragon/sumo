// require("angular-ui-bootstrap");
var ProductControllers = angular.module('productController', ['genericController', 'ui.bootstrap']);

ProductControllers.controller('shopCtrl', ['$scope', '$http', '$routeParams', '$uibModal',
  function ($scope, $http, $routeParams, $uibModal) {
    $scope.order = {quantity: 1, price: 0.00};
    $scope.product = _.find($scope.products, function(prod) { return prod.param == $routeParams.category });
    $scope.$parent.cartTotal = _.reduce($scope.$parent.cart, function(memo, cartItem){ return memo + Number(cartItem.price); }, 0);

    $http.get('/src/app/data/pricing.json').success(function(pricing) {
        $scope.pricing = pricing;
    }); 

    $scope.placeOrder = function(itemId, itemName){
      var cartItem = $scope.order;
      cartItem.id = randomString(5);
      cartItem.name = itemName;
      cartItem.desc = [];

      _.each($scope.product.pricing_config, function(config){
        if(config.name != "unit")
          cartItem.desc.push(config.options[$scope.order[config.name]]);
      })

      cartItem.desc = cartItem.desc.join(", ");
      $scope.$parent.cartTotal += Number(cartItem.price);
      $scope.cart.push(cartItem);
    }

    $scope.newOrder = function(productId){
      $scope.orderItem = _.find($scope.product.items, function(item){ return item.id == productId; })
      $scope.order = {quantity: 1, price: 0.00};
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: '/src/app/template/modal-order-form.html',
        controller: 'genericModalCtrl',
        scope: $scope
      });
    }

    $scope.updatePrice = function(itemId){
      var order = $scope.order
      var pricingConfig = _.sortBy($scope.product.pricing_config, "level")

      var price = $scope.pricing[itemId];
      var priceFound = pricingConfig.every(function(config){
        price = price [order[config.name]];
        return price !== undefined;
      })

      if(!order.quantity)
        $scope.order.price = "-";

      if(priceFound && order.quantity)
        $scope.order.price = price *  Number(order.quantity);
    }

    $scope.deleteCartItem = function(cartItemId){
      $scope.$parent.cart = _.reject($scope.cart, function(cartItem) {return cartItem.id == cartItemId})
      $scope.$parent.cartTotal = _.reduce($scope.cart, function(memo, cartItem){ return memo + Number(cartItem.price); }, 0);
    }

    $scope.invalidForm = function(){
      return $(".ng-invalid").length > 0;
    }
}]);

ProductControllers.controller('productCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams){
    
  }]);

ProductControllers.controller('cartCtrl', ['$scope', '$http', '$routeParams',
  function ($scope, $http, $routeParams){
    $scope.deleteCartItem = function(cartItemId){
      $scope.$parent.cart = _.reject($scope.cart, function(cartItem) {return cartItem.id == cartItemId})
      $scope.$parent.cartTotal = _.reduce($scope.cart, function(memo, cartItem){ return memo + Number(cartItem.price); }, 0);
    }
  }]);

var randomString = function(length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

    if (! length) {
        length = Math.floor(Math.random() * chars.length);
    }

    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}
module.exports = ProductControllers