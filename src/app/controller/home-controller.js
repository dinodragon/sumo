var HomeControllers = angular.module('homeController', ['genericController']);

HomeControllers.controller('HomeCtrl', ['$scope', '$http',
  function ($scope, $http) {
  	$scope.cart = [];
  	$scope.cartTotal = 0.00;
    $http.get('/src/app/data/products.json').success(function(products) {
      	$scope.products = products
    });
  }]);

module.exports = HomeControllers
