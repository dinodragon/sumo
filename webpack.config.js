var path = require('path');
var webpack = require('webpack');

module.exports = {
  context: path.join(__dirname, 'src/app'),
  entry: "./app",
  output: {
    path: __dirname,
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
      { test: /\.(woff|woff2)$/, loader:"url?prefix=font/&limit=5000" },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" }
    ]
  },
  resolve: {
    root: [path.join(__dirname, 'src/assets/scripts'), path.join(__dirname, 'node_module/bootstrap-material-design/scss')],
    extensions: ['', '.js', '.json', '.scss', '.html'],
    alias: {
      _:  'underscore-min'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      // angular: 'angular',
      $: "jquery",
      jQuery: "jquery",
      _: "_"
    })
  ]
};